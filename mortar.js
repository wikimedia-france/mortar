var express = require('express');
var path = require('path');
var logger = require('morgan');
var index = require('./routes/index');
var i18n = require("i18n-express");
var app = express();


// view engine setup
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

// set path for static assets
app.use("/mortar", express.static('public'));

// i18n
app.use(i18n({
  translationsPath: path.join(__dirname, 'i18n'),
  siteLangs: ["en","fr"],
  defaultLang: 'en',
  paramLangName: 'lang',
  textsVarName: 'translation'
}));


// routes
app.use('/mortar', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.render('error', {status:err.status, message:err.message});
});

module.exports = app;